from parser_ import Parser_
from axioms import axioms, scheme_axiom, axiom_induction, axiom_universal, axiom_existence, semi_cons
from substitute import subst, find_mismatch, subst_pred
from math_expressions import Variable, Zero, Increment
from logic_expressions import Consequence, Universal, Existence, Predicate
from util import ConnectedEntryException
from copy import copy
import sys
import time
read = 'test.in'
write = 'test.out'
if len(sys.argv) > 1:
    read = sys.argv[1]
if len(sys.argv) > 2:
    write = sys.argv[2]

parser = Parser_()

read = open(read)
write = open(write, 'w')

uni_rule = open('uni_quantifier.proof')
exist_rule = open('exist_quantifier.proof')


def split_by_comma(assump):
    sum = 0
    last_pos = 0
    ans = []
    for i, a in enumerate(assump):
        if a == '(':
            sum += 1
        elif a == ')':
            sum -= 1
        if sum == 0 and a == ',':
            ans.append(assump[last_pos:i])
            last_pos = i
    if len(ans) == 0 and len(assump) != 0:
        ans.append(assump)
    if len(ans) > 0 and assump[last_pos] == ',':
        ans.append(assump[last_pos:])
    if len(ans) > 1:
        for i in range(1, len(ans)):
            ans[i] = ans[i][1:]
    return ans



parser = Parser_()
proof = []
deduction = []
annot = []
cache = {}
assumptions, formula = read.readline().split('|-')
formula = parser.parse(formula)
assumptions = [parser.parse(assumption) for assumption in split_by_comma(assumptions)]
uni_rule = [parser.parse(line) for line in uni_rule]
exist_rule = [parser.parse(line) for line in exist_rule]
if assumptions != []:
    alpha = assumptions.pop()
else:
    alpha = None


def axiom_deduct(epression):
    cons = subst_pred(scheme_axiom[0], {'A': expression, 'B': alpha})
    deduction.append(cons)
    deduction.append(expression)
    deduction.append(cons.args[1])

correctness = True
message = ''
line_number = 1

t = time.time()
for line in read:
    comment = ''
    proved = False
    expression = parser.parse(line)
    for j, statement in enumerate(axioms):
        if expression == statement:
            proved = True
            comment = 'axiom ' + str(j)
            if alpha is not None:
                axiom_deduct(expression)
            break
    for j, statement in enumerate(assumptions):
        if expression == statement:
            proved = True
            comment = 'assumption ' + str(j)
            if alpha is not None:
                axiom_deduct(expression)
            break
    if not proved:
        for j, axiom in enumerate(scheme_axiom):
            if axiom.has_same_shape(expression):
                proved = True
                comment = 'scheme axiom ' + str(j)
                if alpha is not None:
                    axiom_deduct(expression)
                break
    if not proved:
        if axiom_induction.has_same_shape(expression):
            phi = expression.args[1]
            x = expression.args[0].args[1].var
            inc_x = Increment(copy(x))
            try:
                if subst(phi, x, Zero()) == expression.args[0].args[0] and \
                   subst(phi, x,  inc_x) == expression.args[0].args[1].ex.args[1]:
                    proved = True
                    comment = 'induction'
                    if alpha is not None:
                        axiom_deduct(expression)
            except ConnectedEntryException:
                correctness = False
                message = ''
                break
                pass
    if not proved:
        if axiom_universal.has_same_shape(expression):
            phi = expression.args[0].ex
            x = expression.args[0].var
            try:
                term = find_mismatch(phi, expression.args[1], x)
                if subst(phi, x, term) == expression.args[1]:
                    proved = True
                    comment = 'universal scheme'
                    if alpha is not None:
                        axiom_deduct(expression)
            except ConnectedEntryException:
                correctness = False
                message = 'Терм {0} не свободен для подстановки в формулу {1} вместо {2}'.format(str(term), str(phi), str(x))
                break
                pass
    if not proved:
        if expression == alpha:
            for stat in semi_cons:
                deduction.append(subst_pred(stat, {'A': expression}))
                pass
            proved = True
    if not proved:
        if axiom_existence.has_same_shape(expression):
            phi = expression.args[1].ex
            x = expression.args[1].var
            try:
                term = find_mismatch(phi, expression.args[0], x)
                if subst(phi, x, term) == expression.args[0]:
                    proved = True
                    comment = 'existence scheme'
                    if alpha is not None:
                        axiom_deduct(expression)
            except ConnectedEntryException:
                correctness = False
                message = 'Терм {0} не свободен для подстановки в формулу {1} вместо {2}'.format(str(term), str(phi), str(x))
                break
                pass
    if not proved:
        for stat in reversed(proof):
            if isinstance(stat, Consequence) and stat.args[1] == expression:
                num = cache.get(stat.args[0], None)
                num_c = cache.get(stat, None)
                if num is not None:
                    proved = True
                    comment = 'M. P. ' + str(num_c) + ' ' + str(num)
                    if alpha is not None:
                        cons = subst_pred(scheme_axiom[1], {'A': alpha, 'B':stat.args[0], 'C': expression})
                        deduction.append(cons)
                        deduction.append(cons.args[1])
                        deduction.append(cons.args[1].args[1])
                    break
    if not proved:
        if isinstance(expression, Consequence) and isinstance(expression.args[1], Universal):
            needed = Consequence(expression.args[0], expression.args[1].ex)
            num = cache.get(needed, None)
            x = expression.args[1].var
            free = not subst(expression.args[0], x, Zero()) == expression.args[0]
            if num is not None and not free:
                proved = True
                comment = 'universal rule'
                if alpha is not None:
                    if subst(alpha, x, Zero()) == alpha:
                        for expr in uni_rule:
                            deduction.append(subst_pred(expr, {'A': alpha, 'B': expression.args[0], 'C': expression.args[1].ex, 'x': x}))
                    else:
                        correctness = False
                        message = 'Переменная {0} входит свободно в формулу {1}'.format(str(x), str(alpha))
                        break
            elif free:
                correctness = False
                message = 'Переменная {0} входит свободно в формулу {1}'.format(str(x), str(expression.args[0]))
                break
                # error

    if not proved:
        if isinstance(expression, Consequence) and isinstance(expression.args[0], Existence):
            needed = Consequence(expression.args[0].ex, expression.args[1])
            num = cache.get(needed, None)
            x = expression.args[0].var
            free = not subst(expression.args[1], x, Zero())
            if num is not None and not free:
                proved = True
                comment = 'existence rule'
                if alpha is not None:
                    if subst(alpha, x, Zero()) == alpha:
                        for expr in exist_rule:
                            deduction.append(subst_pred(expr, {'A': alpha, 'B': expression.args[0].ex, 'C': expression.args[1], 'x': x}))
                    else:
                        correctness = False
                        message = 'Переменная {0} входит свободно в формулу {1}'.format(str(x), str(alpha))
                        break
            elif free:
                correctness = False
                message = 'Переменная {0} входит свободно в формулу {1}'.format(str(x), str(expression.args[1]))
                break
    if not proved:
        comment = 'not proved'
        correctness = False
        message = 'не доказанное утверждение'
        break
    proof.append(expression)
    annot.append(comment)
    cache[expression] = len(proof)
    line_number += 1

print(time.time()- t)
if correctness:
    if alpha is None:
        deduction = proof
    else:
        formula = Consequence(alpha, formula)
    write.write('|-' + str(formula) + '\n')
    for i in range(len(deduction)):
        write.write(str(deduction[i]) + '\n')
else:
    write.write('Вывод неверен начиная с формулы номер {0}: [{1}]'.format(str(line_number), message))
