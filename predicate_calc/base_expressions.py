# -*- coding: utf-8 -*-


class LogicExpression:
    pass


class MathExpression:

    def has_same_shape(self, expr):
        return False



class Expression:
    _op_hash = None
    _op = None
    _variables = {}

    def __init__(self):
        self.hash = None

    def __hash__(self):
        pass

    def op_hash(self):
        return type(self)._op_hash

    def op(self):
        return type(self)._op


class ArityExpression(Expression):
    _op_hash = None
    _op = None

    def __init__(self, arity: int):
        super().__init__()
        self.arity = arity
        self.args = list([0]*arity)
        self.name = ''

    def __hash__(self):
        if self.hash is None:
            _hash = 0
            for arg in self.args:
                _hash ^= hash(arg)
            _hash = (_hash ^50371567 + hash(self.name) ^ 25827129) % 4294967295
            self.hash = _hash
        return self.hash

    def __eq__(self, other):
        if isinstance(self, type(other)):
            if self.name == other.name:
                arg_eq = True
                for term1, term2 in zip(self.args, other.args):
                    arg_eq = arg_eq and term1 == term2
                return arg_eq
            else:
                return False
        else:
            return False


class Binary(ArityExpression):
    def __init__(self, left: Expression, right: Expression):
        super().__init__(2)
        self.args[0] = left
        self.args[1] = right

    def isbinary(self):
        return True

    def __str__(self):
        return '({0}{1}{2})'.format(str(self.args[0]), type(self)._op, str(self.args[1]))

    def __hash__(self):
        if self.hash is None:
            self.hash = (hash(self.args[1]) ^ 5037167 + self.op_hash() ^ 4946657 + hash(self.args[1]) ^ 2582729) \
                % 4294967295
        return self.hash

    def __eq__(self, other):
        if isinstance(self, type(other)) and self.op() == other.op():
            return self.args[0] == other.args[0] and self.args[1] == other.args[1]
        else:
            return False

    def has_same_shape(self, expr, evaluation=None):
        if evaluation is None:
            evaluation = {}
        if type(self) == type(expr) and isinstance(self, LogicExpression):
            return self.args[0].has_same_shape(expr.args[0], evaluation) and self.args[1].has_same_shape(expr.args[1], evaluation)
        else:
            return False



class Unary(ArityExpression):
    def __init__(self, un: Expression):
        super().__init__(1)
        self.args[0] = un

    def isbinary(self):
        return False

    def __str__(self):
        return type(self)._op + str(self.args[0])

    def __hash__(self):
        if self.hash is None:
            self.hash = (hash(self.args[0]) ^ 5037167 + self.op_hash() ^ 4946657) % 4294967295
        return self.hash

    def __eq__(self, other):
        if isinstance(self, type(other)) and self.op() == other.op():
            return self.args[0] == other.args[0]
        else:
            return False

    def has_same_shape(self, expr, evaluation=None):
        if evaluation is None:
            evaluation = {}
        if type(self) == type(expr) and isinstance(self, LogicExpression):
            return self.args[0].has_same_shape(expr.args[0], evaluation)
        else:
            return False



class Quantifier(Expression):
    def __init__(self, var, ex: Expression):
        super().__init__()
        self.var = var
        self.ex = ex

    def __str__(self):
        return type(self)._op + str(self.var) + str(self.ex)

    def __hash__(self):
        return (self.op_hash() ^ 5037167 + hash(self.var) ^ 4946657 + hash(self.ex) ^ 2582729) % 4294967295

    def __eq__(self, other):
        if type(self) == type(other):
            return self.var == other.var and self.ex == other.ex
        else:
            return False

    def has_same_shape(self, expr, evaluation=None):
        if evaluation is None:
            evaluation = {}
        if type(self) == type(expr):# and self.var == expr.var:
            return self.ex.has_same_shape(expr.ex, evaluation)
        else:
            return False


