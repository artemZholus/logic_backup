# -*- coding: utf-8 -*-
from base_expressions import Binary, Unary, Expression, ArityExpression, MathExpression


class Addition(MathExpression, Binary):
    _op_hash = hash('+++')
    _op = '+'


class Multiplication(MathExpression, Binary):
    _op_hash = hash('***')
    _op = '*'


class Increment(MathExpression, Unary):
    _op_hash = hash("'''")
    _op = "'"

    def __str__(self):
        expr = self
        ans = ""
        while isinstance(expr, Increment):
            expr = expr.args[0]
            ans += "'"
        return str(expr) + ans


class Variable(MathExpression, Expression):

    def __init__(self, name, free=True):
        super().__init__()
        self.name = name
        self.free = free

    def __str__(self):
        return self.name

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        return type(self) == type(other) and self.name == other.name


class Zero(MathExpression, Expression):

    def __str__(self):
        return '0'

    def __hash__(self):
        return hash('0')

    def __eq__(self, other):
        return type(other) == Zero


class Function(MathExpression, ArityExpression):

    def __init__(self, arity: int, name: str, args: list):
        super().__init__(arity)
        self.args = args
        self.name = name

    def __str__(self):
        return '{0}({1})'.format(self.name, ','.join(map(str, self.args)))


def is_math_expression(ex: Expression):
    return isinstance(ex, MathExpression)
