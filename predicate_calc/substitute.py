# -*- coding: utf-8 -*-
from base_expressions import MathExpression, Expression, Unary, Binary, Quantifier, ArityExpression, LogicExpression
from math_expressions import Variable
from logic_expressions import Predicate
from copy import deepcopy
from util import Multiset, ConnectedEntryException


def subst(expression: Expression, x: MathExpression, y: MathExpression):
    connected = Multiset()

    def has_connected_vars(expr: MathExpression):
        if isinstance(expr, ArityExpression):
            has = False
            for arg in expr.args:
                has = has or has_connected_vars(arg)
        elif isinstance(expr, Variable):
            return expr in connected and not expr == x
        else:
            has = False
        return has

    def dfs(expr: Expression):
        if isinstance(expr, ArityExpression):
            for i, arg in enumerate(expr.args):
                if isinstance(arg, Variable) and arg == x and arg not in connected:
                    if has_connected_vars(y):
                        raise ConnectedEntryException()
                    expr.args[i] = y
                elif not isinstance(arg, Variable):
                    dfs(arg)
        elif isinstance(expr, Quantifier):
            connected.add(expr.var)
            dfs(expr.ex)
            connected.remove(expr.var)
        return expr

    return dfs(deepcopy(expression))


def find_mismatch(expr, s_expr, var):
    if isinstance(expr, ArityExpression) and isinstance(s_expr, type(expr)):
        mismatch = None
        for i, arg in enumerate(expr.args):
            if mismatch is not None:
                break
            else:
                mismatch = find_mismatch(arg, s_expr.args[i], var)
        return mismatch
    elif isinstance(expr, Quantifier) and isinstance(expr, type(s_expr)):
        return find_mismatch(expr.ex, s_expr.ex, var)
    elif isinstance(expr, Variable) and expr == var:
        return s_expr


def subst_pred(expr, map):
    _expr = deepcopy(expr)

    def dfs(expression):
        if isinstance(expression, ArityExpression):
            for i, arg in enumerate(expression.args):
                if isinstance(arg, Predicate):
                    expression.args[i] = map[arg.name]
                elif isinstance(arg, LogicExpression):
                    dfs(arg)
        elif isinstance(expression, Quantifier):
            if isinstance(expression.ex, Predicate):
                if expression.var.name in map:
                    expression.var = map[expression.var.name]
                expression.ex = map[expression.ex.name]
            elif isinstance(expression.ex, LogicExpression):
                dfs(expression.ex)
        return expression

    return dfs(_expr)
