# -*- coding: utf-8 -*-
from sum_proofs import abba, base, last
from copy import deepcopy
from math_expressions import Increment, Zero, Variable
from substitute import subst
import sys
import re

num = re.compile('\d+')

read = 'test.in'
write = 'test.out'
if len(sys.argv) > 1:
    read = sys.argv[1]
if len(sys.argv) > 2:
    write = sys.argv[2]

read = open(read)
write = open(write, 'w')

line = read.readline()
match = num.findall(line)
a = match[0]
b = match[1]
a = int(a)
b = int(b)


def inc_n(term, n):
    return term + "'" * n

write.write('|-{0}+{1}={2}\n'.format(inc_n('0', a), inc_n('0', b), inc_n('0', a+b)))

for line in abba:
    write.write(line)

pat = re.compile(r'o|d')
for i in range(b):
    for expr in base:
        map = {'o': inc_n('0', i), 'd': inc_n('a', i)}
        write.write(pat.sub(lambda x: map[x.group()], expr) + '\n')
    pass

o = inc_n('0', b)
d = inc_n('a', b)
q = inc_n('0', a)
f = inc_n('0', a + b)
map = {'o': o, 'd': d, 'q': q, 'f': f}
patt = re.compile(r'o|d|q|f')
for expr in last:
    write.write(patt.sub(lambda x: map[x.group()], expr) + '\n')
