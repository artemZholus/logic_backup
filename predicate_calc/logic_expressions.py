# -*- coding: utf-8 -*-
from base_expressions import Quantifier, Binary, Unary, ArityExpression, Expression, LogicExpression


class Universal(LogicExpression, Quantifier):
    _op_hash = hash('@')
    _op = '@'


class Existence(LogicExpression, Quantifier):
    _op_hash = hash('?')
    _op = '?'


class Equation(LogicExpression, Binary):
    _op_hash = hash('===')
    _op = '='


class Consequence(LogicExpression, Binary):
    _op_hash = hash('->')
    _op = '->'


class Disjunction(LogicExpression, Binary):
    _op_hash = hash('|||')
    _op = '|'


class Conjunction(LogicExpression, Binary):
    _op_hash = hash('&&&')
    _op = '&'


class Negation(LogicExpression, Unary):
    _op_hash = hash('!!!')
    _op = '!'


class Predicate(ArityExpression, LogicExpression):

    def __init__(self, arity: int, name: str, args: list):
        super().__init__(arity)
        self.args = args
        self.name = name

    def __str__(self):
        if len(self.args) == 0:
            return self.name
        else:
            return '{0}({1})'.format(self.name, ','.join(map(str, self.args)))

    def has_same_shape(self, expr, evaluation=None):
        if evaluation is None:
            evaluation = {}
        if isinstance(expr, LogicExpression):
            if self in evaluation:
                return evaluation[self] == expr
            else:
                evaluation[self] = expr
                return True
        else:
            return False
        pass


def is_logic_expression(ex: Expression):
    return isinstance(ex, LogicExpression)
