# -*- coding: utf-8 -*-
#from parser import Parser
#parser = Parser()

abba = [line for line in open('abba.proof')]

base = [
    "a+b'=(a+b)'",
    "a+b'=(a+b)'->(!!A->A)->a+b'=(a+b)'",
    "(!!A->A)->a+b'=(a+b)'",
    "(!!A->A)->@b(a+b'=(a+b)')",
    "!!A->A",
    "@b(a+b'=(a+b)')",
    "@b(a+b'=(a+b)')->(a+o'=(a+o)')",
    "a+o'=(a+o)'",
    "@x@y(x=y->y=x)->@y((a+o')=y->y=(a+o'))",
    "@y((a+o')=y->y=(a+o'))",
    "@y((a+o')=y->y=(a+o'))->(a+o')=(a+o)'->(a+o)'=(a+o')",
    "(a+o')=(a+o)'->(a+o)'=(a+o')",
    "(a+o)'=(a+o')",# 1
    "a=b->a=c->b=c",
    "(a=b->a=c->b=c)->(!!A->A)->(a=b->a=c->b=c)",
    "(!!A->A)->(a=b->a=c->b=c)",
    "(!!A->A)->@c(a=b->a=c->b=c)",
    "(!!A->A)->@b@c(a=b->a=c->b=c)",
    "(!!A->A)->@a@b@c(a=b->a=c->b=c)",
    "(!!A->A)",
    "@a@b@c(a=b->a=c->b=c)",
    "@a@b@c(a=b->a=c->b=c)->@b@c((a+o)'=b->(a+o)'=c->b=c)",
    "@b@c((a+o)'=b->(a+o)'=c->b=c)",
    "@b@c((a+o)'=b->(a+o)'=c->b=c)->@c((a+o)'=(a+o')->(a+o)'=c->(a+o')=c)",
    "@c((a+o)'=(a+o')->(a+o)'=c->(a+o')=c)",
    "@c((a+o)'=(a+o')->(a+o)'=c->(a+o')=c)->((a+o)'=(a+o')->(a+o)'=d'->(a+o')=d')",
    "((a+o)'=(a+o')->(a+o)'=d'->(a+o')=d')",
    "(a+o)'=d'->(a+o')=d'",
    "a+o=d",
    "a=b->a'=b'",
    "(a=b->a'=b')->(!!A->A)->(a=b->a'=b')",
    "(!!A->A)->(a=b->a'=b')",
    "(!!A->A)->@b(a=b->a'=b')",
    "(!!A->A)->@a@b(a=b->a'=b')",
    "@a@b((a=b)->(a'=b'))",
    "@a@b((a=b)->(a'=b'))->@b((a+o=b)->(a+o)'=b')",
    "@b((a+o=b)->(a+o)'=b')",
    "@b((a+o=b)->(a+o)'=b')->((a+o=d)->(a+o)'=d')",
    "(a+o=d)->(a+o)'=d'",
    "(a+o)'=d'",
    "a+o'=d'"
]

# base = [parser.parse(line) for line in base]

last = [
    "a+o=d",
    "(a+o=d)->(!!A->A)->(a+o=d)",
    "(!!A->A)->(a+o=d)",
    "(!!A->A)->@a(a+o=d)",
    "(!!A->A)",
    "@a(a+o=d)",
    "@a(a+o=d)->(q+o=f)",
    "q+o=f"
]

# last = [parser.parse(line) for line in last]

# "a+0=a"
# "a+0'=(a+0)'"
# "a+0'=a'"
#
# "(a=b->a=c->b=c)[a:=(a+0)', b:=a+0', c:=a']"
# "(a+0)'=a+0'->(a+0)'=a'->a+0'=a' "
# "(a+0)'=a+0'"
# "(a+0)'=a'->a+0'=a' "
# "a+0=a"
# "a+0=a->(a+0)'=a'"
# "(a+0)'=a' "
# "a+0'=a' "
#
# "a+b=c"
# "a+b'=c' "
