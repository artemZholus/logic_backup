# -*- coding: utf-8 -*-
import re
from util import Token, const_char_token, is_logic_symbol, is_math_symbol, is_multi_char_token, \
    const_char_symbols, const_char_tokenizer, const_char_characterizer
from logic_expressions import Equation, Universal, Conjunction, Consequence, Disjunction, Predicate, Negation, Existence
from math_expressions import Variable, Function, Addition, Multiplication, Increment, Zero, is_math_expression
from copy import copy


class ParseException(Exception):
    def __init__(self):
        super().__init__()


class Parser_:
    __predicate_pattern = re.compile(r'[A-Z]+[0-9]*')
    __function_pattern = re.compile(r'[a-z]+[0-9]*')

    def __init__(self, formula='', axiom=False):
        self.current_str = formula
        self.axiom = axiom
        self.current_word = ''
        self.current_token = Token.NONE
        self.index = 0
        self.free_vars = {}

    def reset_state(self):
        self.current_str = ''
        self.axiom = False
        self.current_word = ''
        self.current_token = Token.NONE
        self.index = 0
        self.free_vars = {}

    def parse(self, formula, axiom=False):
        self.reset_state()
        self.current_str = formula
        self.axiom = axiom
        return self.expr()

    def parse_term(self, term):
        self.reset_state()
        self.current_str = term
        self.axiom = False
        return self.term()

    def next_token(self):
        if self.current_token == Token.END:
            return
        if self.index == len(self.current_str):
            self.current_token = Token.END
        elif self.current_str[self.index] in const_char_tokenizer:
            self.current_token = const_char_tokenizer[self.current_str[self.index]]
            self.index += len(const_char_characterizer[self.current_token])
        elif self.current_str[self.index].isalpha():
            if self.current_str[self.index].isupper():
                self.current_token = Token.PRED
                match = Parser_.__predicate_pattern.search(self.current_str, self.index)
                self.index = match.end()
                self.current_word = match.group()
            else:
                match = Parser_.__function_pattern.search(self.current_str, self.index)
                if match.end() < len(self.current_str) and self.current_str[match.end()] == '(' and \
                        not (self.current_token == Token.UNIVERSAL or self.current_token == Token.EXISTENCE):
                    self.current_token = Token.FUNC
                else:
                    self.current_token = Token.VAR
                self.current_word = match.group()
                self.index = match.end()
        pass

    def save_state(self):
        return self.current_token, self.current_word, self.index, copy(self.free_vars)

    def load_state(self, ctok, cword, ind, fvars):
        self.current_token = ctok
        self.current_word = cword
        self.index = ind
        self.free_vars = fvars

    def after(self):
        ctok, cword, ind, fvars = self.save_state()
        self.next_token()
        token = self.current_token
        self.load_state(ctok, cword, ind, fvars)
        return token

    def expr(self):
        ex = self.disj()
        if self.current_token == Token.CONS:
            ex = Consequence(ex, self.expr())
        return ex

    def disj(self):
        ex = self.conj()
        while self.current_token == Token.OR:
            ex = Disjunction(ex, self.conj())
        return ex

    def conj(self):
        ex = self.unary()
        while self.current_token == Token.AND:
            ex = Conjunction(ex, self.unary())
        return ex

    def unary(self):
        token = self.after()
        if token == Token.NOT:
            self.next_token()
            ex = Negation(self.unary())
        elif token == Token.OBR:
            ctok, cword, ind, fvars = self.save_state()
            try:
                self.next_token()
                ex = self.expr()
                if is_math_expression(ex):
                    raise ParseException
                self.next_token()
            except (UnboundLocalError, ParseException) as e:
                self.load_state(ctok, cword, ind, fvars)
                ex = self.pred()
        elif token == Token.UNIVERSAL or token == Token.EXISTENCE:
            # quantifier = self.current_token
            quantifier = token
            self.next_token()
            self.next_token()
            self.free_vars[self.current_word] = False
            var = Variable(self.current_word, False)
            if quantifier == Token.EXISTENCE:
                ex = Existence(var, self.unary())
            else:
                ex = Universal(var, self.unary())
            self.free_vars[var.name] = True
        else:
            ex = self.pred()
        return ex

    def pred(self):
        if self.after() == Token.PRED:
            self.next_token()
            args = []
            pred_name = self.current_word
            if self.after() == Token.OBR:
                self.next_token()
                while self.current_token != Token.CBR:
                    args.append(self.term())
            ex = Predicate(len(args), pred_name, args)
            self.next_token()
        else:
            ex = self.term()
            ex = Equation(ex, self.term())
        return ex

    def term(self):
        ex = self.add()
        while self.current_token == Token.ADD:
            ex = Addition(ex, self.add())
        return ex

    def add(self):
        ex = self.mul()
        while self.current_token == Token.MUL:
            ex = Multiplication(ex, self.mul())
        return ex

    def mul(self):
        self.next_token()
        if self.current_token == Token.OBR:
            ex = self.term()
            self.next_token()
        elif self.current_token == Token.ZERO:
            ex = Zero()
            self.next_token()
        elif self.current_token == Token.VAR:
            free = self.free_vars.setdefault(self.current_word, True)
            ex = Variable(self.current_word, free)
            self.next_token()
        elif self.current_token == Token.FUNC:
            # function
            args = []
            func_name = self.current_word
            self.next_token()
            if self.current_token == Token.OBR:
                while self.current_token != Token.CBR:
                    args.append(self.term())
            ex = Function(len(args), func_name, args)
            self.next_token()
        while self.current_token == Token.INC:
            ex = Increment(ex)
            self.next_token()
        return ex


# st = "(a+0=a'''')|@x?yP(x,f(x,y),y)->x=x&?xP(x,x)"
# # less = "?c(a+c=b&!c=0)" # a < b
# old = 'A->B->A'
# lim = "@epsilon(less[a:=epsilon,b:=0]|(?n0@n(less[a:=n,b:=n0])|less[a:=xn,b:=x+epsilon]&less[a:=x,b:=xn+epsilon]))"
# # st = '@xP(x)'
# lim = "@epsilon((?c(epsilon+c=0&!c=0))|?n0@n((?c(n+c=n0&!c=0))|(?c(x(n)+c=x+epsilon&!c=0))&(?c(x+c=x(n)+epsilon&!c=0))))"
# e = p.parse(st)
# # e = p.parse(lim)
# print(str(e))
# print(hash(e))
