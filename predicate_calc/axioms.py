# -*- coding: utf-8 -*-
from parser_ import Parser_

parser = Parser_()
axioms = [
    "a=b->a'=b'",
    "a=b->a=c->b=c",
    "a'=b'->a=b",
    "!a'=0",
    "a+b'=(a+b)'",
    "a+0=a",
    "a*0=a",
    "a*b'=a*b+a"
]
axioms = [parser.parse(axiom) for axiom in axioms]

scheme_axiom = [
    'A->B->A',  # 1
    '(A->B)->(A->B->C)->(A->C)',  # 2
    'A->B->A&B',  # 3
    'A&B->A',  #4
    'A&B->B',  #5
    'A->A|B',  #6
    'B->A|B',  #7
    '(A->B)->(C->B)->(A|C->B)',  #8
    '(A->B)->(A->!B)->!A',  #9
    '!!A->A'
]  # 10

semi_cons = [
    'A->A->A',
    '(A->A->A)->(A->(A->A)->A)->(A->A)',
               '(A->(A->A)->A)->(A->A)',
                'A->(A->A)->A',
                                'A->A'
]

semi_cons = [parser.parse(line) for line in semi_cons]

axiom_induction = "F(0)&@x(F(x)->F(x'))->F(x)"
axiom_universal = "@xF(x)->F(o)"
axiom_existence = "F(o)->?xF(x)"

axiom_induction = parser.parse(axiom_induction)
axiom_universal = parser.parse(axiom_universal)
axiom_existence = parser.parse(axiom_existence)

scheme_axiom = [parser.parse(axiom) for axiom in scheme_axiom]
