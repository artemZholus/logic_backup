# -*- coding: utf-8 -*-


class ConnectedEntryException(Exception):
    def __init__(self):
        super().__init__()


class Multiset(dict):

    def __init__(self):
        super().__init__()

    def add(self, obj):
        count = self.setdefault(obj, 0)
        self[obj] = count + 1
        pass

    def remove(self, obj):
        if obj in self:
            self[obj] -= 1
            if self[obj] == 0:
                del self[obj]


class Token:
    END = -1
    NONE = -2
    OBR = 0
    CBR = 1
    VAR = 2
    PRED = 3
    FUNC = 4
    ADD = 5
    MUL = 6
    INC = 7
    EQ = 8
    CONS = 9
    AND = 10
    OR = 11
    NOT = 12
    EXISTENCE = 13
    UNIVERSAL = 14
    ZERO = 15
    COMMA = 16

math_symbols = [Token.MUL, Token.ADD, Token.FUNC, Token.VAR, Token.ZERO, Token.INC]
logic_symbols = [Token.EXISTENCE, Token.AND, Token.CONS, Token.EQ, Token.NOT, Token.OR, Token.PRED, Token.UNIVERSAL]

spec_symbols = [Token.OBR, Token.CBR, Token.END, Token.NONE]
const_char_token = [Token.COMMA, Token.OBR, Token.CBR, Token.UNIVERSAL, Token.EXISTENCE, Token.OR, Token.AND,
                    Token.NOT, Token.ADD, Token.MUL, Token.INC, Token.ZERO, Token.EQ, Token.END, Token.CONS]
const_char_symbols = [',', '(', ')', '@', '?', '|', '&', '!', '+', '*', "'", '0', '=', '\n']
const_char_tokenizer = dict(zip(const_char_symbols + ['-'], const_char_token))
const_char_characterizer = dict(zip(const_char_token, const_char_symbols + ['->']))

def is_multi_char_token(token: Token):
    return token not in const_char_token and token not in spec_symbols


def is_math_symbol(token: Token):
    return token in math_symbols


def is_logic_symbol(token: Token):
    return token in logic_symbols