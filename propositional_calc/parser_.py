# -*- coding: utf-8 -*-
from axioms import axioms
from expressions import Disjunction, Conjunction, Consequence, Negation
from variable import Variable
from util import Dictionary, Tokens
import re


class Parser_:
    __pattern = re.compile(r"[a-zA-Z]+[0-9]*")
    tokens = {'->': 0, '|': 1, '&': 2, '!': 3, '(': 4, ')': 5}

    def __init__(self, formula='', axiom=False):
        self.axiom = axiom
        self.curr_str = formula
        self.curr_word = ''
        self.variables = Dictionary()
        self.index = 0
        self.curr_tok = ''

    def set_formula(self, formula, axiom=False):
        self.axiom = axiom
        self.curr_str = formula
        self.variables = Dictionary()
        self.index = 0

    def next_token(self):
        if self.index == len(self.curr_str):
            self.curr_tok = Tokens.END
        elif self.curr_str[self.index].isalpha():
            self.curr_tok = Tokens.VAR
            match = Parser_.__pattern.search(self.curr_str, self.index)
            self.index = match.end()
            self.curr_word = match.group()
        else:
            for key in Parser_.tokens:
                if key.startswith(self.curr_str[self.index]):
                    self.curr_tok = Parser_.tokens[key]
                    self.index += len(key)
                    break

    def expr(self):
        ex = self.disj()
        if self.curr_tok == Tokens.CONS:
            ex = Consequence(ex, self.expr())
        return ex

    def disj(self):
        ex = self.conj()
        while self.curr_tok == Tokens.OR:
            ex = Disjunction(ex, self.conj())
        return ex

    def conj(self):
        ex = self.neg()
        while self.curr_tok == Tokens.AND:
            ex = Conjunction(ex, self.neg())
        return ex

    def neg(self):
        self.next_token()
        if self.curr_tok == Tokens.OPN_BR:
            ex = self.expr()
            self.next_token()
        elif self.curr_tok == Tokens.NOT:
            ex = Negation(self.neg())
        else:
            i = self.variables[self.curr_word]
            if i is None:
                i = len(self.variables) + 1
                self.variables[self.curr_word] = i
            ex = Variable(self.curr_word, i, inAxiom=True if self.axiom else False)
            self.next_token()
        return ex

    def parse(self, formula='', axiom=False):
        if formula != '':
            self.set_formula(formula, axiom)

        class EmptyFormulaException(Exception):
            def __init__(self):
                Exception.__init__(self)

        if self.curr_str == '':
            raise EmptyFormulaException()
        return self.expr()


p = Parser_()
parsed_axioms = []
for i, s in enumerate(axioms):
    p.set_formula(s, axiom=True)
    parsed_axioms.append((p.parse(), i + 1))
    p.set_formula('')
