# -*- coding: utf-8 -*-
from parser_ import parsed_axioms, Parser_
from copy import deepcopy
from deduction import deduction
from expressions import Negation, eval_by_mapping
from variable import Variable
from proofs import simpleProves
import re
import time
import sys

read = 'test.in'
write = 'test.out'
if len(sys.argv) > 1:
    read = sys.argv[1]
if len(sys.argv) > 2:
    write = sys.argv[2]


fr = open(read)
fw = open(write, 'w')
parser = Parser_()


def assumption_pop(assumptions, alpha, formula, old_proof_negated, old_proof):
    proof = deduction(assumptions, alpha, old_proof)  # Вывод Г |- alpha -> formula - (1)
    proof.extend(deduction(assumptions, Negation(alpha), old_proof_negated))  # Вывод Г |- !alpha -> formula - (2)
    proof.extend(
        [eval_by_mapping(line, {'A': alpha}) for line in simpleProves['|-A|!A']]
    )  # док-во alpha|!alpha - (3)
    mapping = {'A': alpha, 'B': formula, 'C': Negation(alpha)}
    proof.append(eval_by_mapping(parsed_axioms[7][0], mapping))  # сх. аксиом 8 - (4)
    proof.append(eval_by_mapping(parsed_axioms[7][0].right, mapping))  # M.P. последнее выражение в (1), (4)  - (5)
    proof.append(eval_by_mapping(parsed_axioms[7][0].right.right, mapping))  # M.P. последнее выражение в (2), (5) - (6)
    proof.append(eval_by_mapping(parsed_axioms[7][0].right.right.right, mapping))  # M.P. последнее выражение в (3), (6)
    return proof


def masks(n):
    for i in range(1 << n):
        mask = [i & (1 << j) != 0 for j in range(n)]
        yield mask


def disproof_test(formula):
    variables = list(set(re.findall(r'\w+\d*', str(formula))))
    for mask in masks(len(variables)):
        _map = dict(zip(variables, mask))
        if not formula.eval(_map):
            return _map
    return {}


def proove(formula):
    fa = re.findall(r'\w+\d*', str(formula))
    fc = []
    for i in fa:
        if i not in fc:
            fc.append(i)
    variables = [parser.parse(i) for i in fc]
    var_mapping = {}
    proof = []

    def proving_dfs(expression):
        if not isinstance(expression.left, Variable):
            proving_dfs(expression.left)
        if expression.right is not None and not isinstance(expression.right, Variable):
            proving_dfs(expression.right)
        if isinstance(expression, Negation):
            if var_mapping[expression.left]:
                key = 'A|-!!A'
            else:
                key = '!A|-!A'
            proof_pattern = simpleProves[key]
            proof.extend([eval_by_mapping(expr, {'A': expression.left}) for expr in proof_pattern])
            i = 0
            neg = proof[-1]
            while isinstance(neg, Negation):
                i += 1
                neg = neg.left
            i = i % 2 == 0
            var_mapping[proof[-1]] = var_mapping[neg] if i else not var_mapping[neg]
            var_mapping[proof[-1].left] = not var_mapping[proof[-1]]
        else:
            asmp_a = expression.left if expression.left in var_mapping else Negation(expression.left)
            asmp_b = expression.right if expression.right in var_mapping else Negation(expression.right)
            truth_table = {'A': var_mapping[asmp_a], 'B': var_mapping[asmp_b]}
            asmp_a = 'A' if truth_table['A'] else '!A'
            asmp_b = 'B' if truth_table['B'] else '!B'
            statement = ('' if expression.op_eval(truth_table) else '!') + '(A{}B)'.format(expression.op())
            key = asmp_a + ',' + asmp_b + '|-' + statement
            proof_pattern = simpleProves[key]
            proof.extend([eval_by_mapping(expr, {'A': expression.left, 'B': expression.right}) for expr in proof_pattern])
            var_mapping[expression] = expression.op_eval(truth_table)
            if isinstance(proof[-1], Negation):
                var_mapping[proof[-1]] = not var_mapping[expression]
        pass

    i = 0
    masked_proofs = list(range(1 << len(variables)))

    for mask in masks(len(variables)):
        var_mapping = dict(zip(variables, mask))
        proof = []
        proving_dfs(formula)
        masked_proofs[i] = deepcopy(proof)
        i += 1
    pass


    def debug_test():
        # DEBUG
        k = 0
        for mask in masks(len(variables)):
            fw.write(str([str(z) for z in variables]) + '\n')
            fw.write(str(mask) + '\n')
            fw.write(','.join([('' if mask[ind] else '!') + str(z) for ind, z in enumerate(variables)]) + '|-' + str(formula) + '\n')
            for j in masked_proofs[k]:
                fw.write(str(j) + '\n')
            fw.write('\n')
            k += 1
        return

    proof = []
    # debug_test()
    # return
    n = len(variables)
    for j in range(n):
        alpha = variables[0]
        popped = list(variables)
        del popped[0]
        tmp = []
        for i in range(0, len(masked_proofs), 2):
            tmp.append(
                assumption_pop([Negation(t) if i & (1 << (ind + 1)) == 0 else t for ind, t in enumerate(popped)],
                               alpha,
                               formula,
                               masked_proofs[i],
                               masked_proofs[i + 1])
            )
        masked_proofs = tmp
        variables = popped
        # debug_test()
        # return

    return masked_proofs[0]


t = time.time()
parser.set_formula('')
line = fr.read()
ex = parser.parse(line)
q = disproof_test(ex)
if q != {}:
    fw.write('Высказывание ложно при ')
    for key in q:
        fw.write('{0}={1} '.format(str(key), 'И' if q[key] else 'Л'))
else:
    l = proove(ex)
    fw.write('|-{}\n'.format(str(ex)))
    for line in l:
        fw.write(str(line) + '\n')
    print('prooving time: {} sec.'.format(str(time.time() - t)))
