# -*- coding: utf-8 -*-
from parser_ import parsed_axioms, p
from expressions import Consequence
from util import Dictionary
import time
import sys

read = 'test.in'
write = 'test.out'
if len(sys.argv) > 1:
    read = sys.argv[1]
if len(sys.argv) > 2:
    write = sys.argv[2]

fr = open(read)
fw = open(write, 'w')
proof = []
cache = Dictionary()
assumptions = formula = None
assumptions, formula = fr.readline().split('|-')

if not assumptions == '':
    assumptions = [(p.parse(expr, axiom=False), i + 1) for i, expr in enumerate(assumptions.split(','))]
else:
    assumptions = []

t = time.time()
correct = True
for line in fr:
    line = line[:-1]
    if line is '':
        continue
    expr = p.parse(line)
    if len(proof) == 36:
        pass
    fw.write('({0}) {1}'.format(len(proof) + 1, line))
    proved = False
    for assumption, i in assumptions:
        if assumption == expr:
            fw.write(' (предп. {0})\n'.format(i))
            proved = True
            break
    if not proved:
        for axiom, i in parsed_axioms:
            if axiom == expr:
                fw.write(' (сх. акс. {0})\n'.format(i))
                proved = True
                break
    if not proved:
        for cons, id_cons in reversed(proof):
            if isinstance(cons, Consequence) and expr == cons.right:
                id_left = cache[cons.left]
                if id_left is not None:
                    fw.write(' (M.P. {0}, {1})\n'.format(id_left, id_cons))
                    break
        else:
            correct = False
            fw.write(' (Не доказано)\n')
    cache[expr] = len(proof) + 1
    proof.append((expr, len(proof) + 1))
print('checking time: {} sec.'.format(time.time() - t))
if correct:
    print('proof is correct')
else:
    print('proof is incorrect')
