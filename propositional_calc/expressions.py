# -*- coding: utf-8 -*-
from variable import Variable
from axioms import axioms
from copy import deepcopy


class Expression:
    __op_hash = None

    def __init__(self, left, right=None):
        self.left = left
        self.right = right
        self.hash = None
        #self.hash = hash(self)

    def is_binary(self):
        return self.right is not None

    def eq_shape(self, other):
        first = True
        second = True
        if isinstance(other, type(self)):
            first = self.left.eq_shape(other.left)
            if self.is_binary():
                second = self.right.eq_shape(other.right)
        else:
            return False
        return first and second

    def is_axiom(self):
        return self.left.is_axiom()

    def __eq__(self, other):
        if isinstance(other, Expression) and other.is_axiom():
            if self.is_axiom():
                return axioms.index(str(self)) == axioms.index(str(other))
            else:
                return other == self
        b = self.eq_shape(other)
        Variable.expressions.clear()
        return b

    def __hash__(self):
        if self.hash is None:
            self.hash = (hash(self.left) ^ 5037167 + self.op_hash() ^ 4946657 + hash(self.right) ^ 2582729) \
                        % 4294967295
        return self.hash


class Consequence(Expression):
    __op_hash = hash('->')
    __op = '->'

    def __init__(self, left, right):
        Expression.__init__(self, left, right)

    def op_hash(self):
        return Consequence.__op_hash

    def op(self):
        return Consequence.__op

    def eval(self, var_map):
        return not self.left.eval(var_map) or self.right.eval(var_map)

    def __str__(self):
        return '({0}->{1})'.format(str(self.left), str(self.right))

    def op_eval(self, mapping):
        return not mapping['A'] or mapping['B']


class Disjunction(Expression):
    __op_hash = hash('|||')
    __op = '|'

    def __init__(self, left, right):
        Expression.__init__(self, left, right)

    def op_hash(self):
        return Disjunction.__op_hash

    def op(self):
        return Disjunction.__op

    def eval(self, var_map):
        return self.left.eval(var_map) or self.right.eval(var_map)

    def __str__(self):
        return '({0}|{1})'.format(str(self.left), str(self.right))

    def op_eval(self, mapping):
        return mapping['A'] or mapping['B']


class Conjunction(Expression):
    __op_hash = hash('&&&')
    __op = '&'

    def __init__(self, left, right):
        Expression.__init__(self, left, right)

    def op_hash(self):
        return Conjunction.__op_hash

    def op(self):
        return Conjunction.__op

    def eval(self, var_map):
        return self.left.eval(var_map) and self.right.eval(var_map)

    def __str__(self):
        return '({0}&{1})'.format(str(self.left), str(self.right))

    def op_eval(self, mapping):
        return  mapping['A'] and mapping['B']


class Negation(Expression):
    __op_hash = hash('!!!')
    __op = '!'

    def __init__(self, left):
        Expression.__init__(self, left)

    def op_hash(self):
        return Negation.__op_hash

    def op(self):
        return Negation.__op

    def eval(self, var_map):
        return not self.left.eval(var_map)

    def __str__(self):
        return '!' + str(self.left)

    def op_eval(self, mapping):
        return not mapping['A']


def reset_hash(expr):
    expr.hash = None
    if isinstance(expr, Expression):
        reset_hash(expr.left)
        if expr.right is not None:
            reset_hash(expr.right)


def eval_by_mapping(expression, mapping):
    _expr = deepcopy(expression)
    reset_hash(_expr)
    def dfs(expr):
        if isinstance(expr, Variable):
            return mapping[expr.name]
        if not isinstance(expr.left, Variable):
            dfs(expr.left)
        else:
            if expr.left.name in mapping:
                expr.left = mapping[expr.left.name]
        if not isinstance(expr, Negation) and not isinstance(expr.right, Variable):
            dfs(expr.right)
        elif isinstance(expr.right, Variable):
            if expr.right.name in mapping:
                expr.right = mapping[expr.right.name]
        return expr
    return dfs(_expr)
