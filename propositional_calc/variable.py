import copy

class Variable:
    expressions = {}

    def __init__(self, name, num, inAxiom=False):
        self.name = name
        self.num = num
        self.inAxiom = inAxiom
        self.hash = None
        self.hash = hash(self)

    def __hash__(self):
        if self.hash is None:
            self.hash = hash(self.name) % 9223372036854769231
        return self.hash

    def __eq__(self, other):
        return isinstance(other, Variable) and self.name == other.name

    def is_axiom(self):
        return self.inAxiom

    def eval(self, var_map):
        return var_map[self.name]

    def eq_shape(self, other):
        if self.inAxiom:
            if not self.num in Variable.expressions.keys():
                Variable.expressions[self.num] = other
            if Variable.expressions[self.num].eq_shape(other):
                return True
            else:
                return False
        elif isinstance(other, Variable):
            return self.name == other.name
        else:
            return False

    def __str__(self):
        return self.name
