# -*- coding: utf-8 -*-
from parser_ import p, parsed_axioms
from expressions import Expression, Consequence, eval_by_mapping
from variable import Variable
import sys
import time


def deduction(assump, alpha, old_proof):
    cache = {}
    self_consequence = ['A->A->A',
                       '(A->A->A)->(A->(A->A)->A)->(A->A)',
                                  '(A->(A->A)->A)->(A->A)',
                                   'A->(A->A)->A',
                                                   'A->A']
    self_consequence = [p.parse(i) for i in self_consequence]
    res = []
    proof = []
    j = 0
    for line in old_proof:
        if j == 34:
            pass
        for exp in assump + parsed_axioms:
            if (isinstance(exp, tuple) and exp[0] == line) or \
                    ((isinstance(exp, Expression) or isinstance(exp, Variable)) and exp == line):
                res.append(Consequence(line, Consequence(alpha, line)))
                res.append(line)
                res.append(Consequence(alpha, line))
                break
        else:
            if line == alpha:
                for i in self_consequence:
                    res.append(eval_by_mapping(i, {'A': alpha}))
            else:
                for cons, i in reversed(proof):
                    if isinstance(cons, Consequence) and cons.right == line and cons.left in cache:
                        q = {'A': alpha, 'B': cons.left, 'C': line}
                        res.append(eval_by_mapping(parsed_axioms[1][0], q))
                        res.append(eval_by_mapping(parsed_axioms[1][0].right, q))
                        res.append(Consequence(alpha, line))
                        break
        proof.append((line, len(proof) + 1))
        cache[line] = len(proof)
        j += 1
    return res


if __name__ == '__main__':
    read = 'test.in'
    write = 'test.out'
    if len(sys.argv) > 1:
        read = sys.argv[1]
    if len(sys.argv) > 2:
        write = sys.argv[2]
    fr = open(read)
    fw = open(write, 'w')
    assumptions, formula = fr.readline().split('|-')
    assumptions = assumptions.split(',')
    formula = p.parse(formula)
    alpha = p.parse(assumptions.pop())
    fw.write('{0}|-{1}->{2}\n'.format(','.join(assumptions), str(alpha), str(formula)))
    assumptions = [p.parse(expr) for expr in assumptions]
    t = time.time()
    old_proof = []
    for line in fr:
        if line != '\n':
            old_proof.append(p.parse(line))
    res = deduction(assumptions, alpha, old_proof)
    for i in res:
        fw.write(str(i) + '\n')
    print(time.time() - t)

# m = {'a': Variable('q', 1), 'b': Variable(p, 2)}
# print(str(eval_by_mapping(a, m)))
