class Dictionary(dict):
    def __missing__(self, key):
        return None


class Tokens:
    CONS = 0
    OR = 1
    AND = 2
    NOT = 3
    OPN_BR = 4
    CLS_BR = 5
    VAR = 6
    END = 7
