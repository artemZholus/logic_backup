import re


def Z(x):
    return 0


def N(x):
    return x + 1


def pi(i):
    return lambda *args: args[i - 1]


def S(f, *g):
    return lambda *args: f(*map(lambda x: x.__call__(*args), g))


def R(f, g):
    return lambda y, *args: f(*args) if y == 0 else g(y - 1, R(f, g)(y - 1, *args), *args)


def mu(f, y=0):
    return lambda *args: y if f(y, *args) == 0 else mu(f, y + 1)(*args)


def parse(str):
    m = {'<': '(', '>': ')'}
    str = re.sub('\<|\>', lambda x: m[x.group()], str)
    return eval(str)


add = parse('R<pi<1>, S<N, pi<2>>>')
mul = parse('R<Z, S<add, pi<2>, pi<3>>>')

